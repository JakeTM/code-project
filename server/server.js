const path = require('path');
const express = require('express');
const app = express();             
const port = 5000; 
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//run production build on server
app.use(express.static(path.join(__dirname, '../client/build')));
//routes built app page on server (localhost:5000)
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, '../client/build/index.html'));
});

//json data (treated like a database)
const data = require(path.join(__dirname, 'data/templates.json'));

//API for getting the selected Details
app.get("/api/getDetailsById", (req, res) => {
    result = data.find(item => item.id == req.query.id);
    result !== undefined ? res.json(result) : res.sendStatus(404);
});

//API for getting the first available Details Id
app.get("/api/getFirstDetailsId", (req, res) => {
    result = data.find(item => true);
    result !== undefined ? res.json(result.id) : res.sendStatus(404);
});

//API for getting the thumbnails from data based on pagination
app.get("/api/getThumbnailsByPage", (req, res) => {
    var result = []
    if ((data.find(item => true)) && req.query.page > 0){
        var firstThumbnailIndex = (req.query.page - 1) *4;
        var totalThumbnails = Object.keys(data).length;
        for(var i = firstThumbnailIndex;i < totalThumbnails && i < firstThumbnailIndex + 4;i++){
            result.push({ id : data[i].id, thumbnail: data[i].thumbnail, active: false })
        }
        res.json(result);
    }
    else {res.sendStatus(404);}
});

//API for getting total nmber of pages (needed for Next button in Filmstrip)
app.get("/api/getNumPages", (req, res) => {
    var totalPages = +Object.keys(data).length / 4;
    res.json(totalPages);
});

//Hard coding production for submission and compatability with  Windows/Mac/Linux
//swappable values: development, production, test 
process.env.NODE_ENV = 'production';
if(process.env.NODE_ENV !== 'test'){
    module.exports = app.listen(port, () => {
        console.log(`Now listening on port ${port}`); 
    });
}
else{
    module.exports = app;
}


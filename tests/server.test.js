const request = require('supertest');
const app = require('../server/server');
const path = require('path');
//json data (treated like a database) (queried for test data to query)
const data = require(path.join(__dirname, '../server/data/templates.json'));

describe('Details', () => {
    describe('getDetailsById', () => {
        it('GET /api/getDetailsById/ --> Json of Details', () => {
            return request(app).get('/api/getDetailsById')
            .query({ id: data[0].id })
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).toEqual(
                    {
                        title: expect.any(String),
                        cost: expect.any(String),
                        id: expect.any(String),
                        description: expect.any(String),
                        thumbnail: expect.any(String),
                        image: expect.any(String),
                    }
                );
            })
        });
    
        it('GET /api/getDetailsById/ --> 404 if not found', () => {
            return request(app).get('/api/getDetailsById')
            .query({ id: '' })
            .expect(404);
        });
    
        it('GET /api/getDetailsById/ DESTRUCTIVE QUERY --> 404', () => {
            return request(app).get('/api/getDetailsById')
            .query({ id: true , extraArgument: 172864271827456, id: 83738570375915 })
            .expect(404);
        });
    
        it('GET /api/getDetailsById/ TIMEOUT 200MS --> Json of Details before 200ms', () => {
            return request(app).get('/api/getDetailsById')
            .query({ id: data[0].id })
            .timeout(200)
            .expect('Content-Type', /json/)
            .expect(200)
        });
    })
    
    describe('getFirstDetailsId', () => {
        it('GET /api/getFirstDetailsId/ --> Json containing first details id from templates.json', () => {
            return request(app).get('/api/getFirstDetailsId')
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).toEqual(expect.any(String));
            })
        });
    
        it('GET /api/getFirstDetailsId/ --> 404 if templates.json empty', () => {
            if (Object.keys(data).length === 0 || Object.keys(data).length === undefined){
                return request(app).get('/api/getFirstDetailsId')
                .expect(404);
            }
        });

        it('GET /api/getFirstDetailsId/ DESTRUCTIVE QUERY --> 200, no arguments required', () => {
            return request(app).get('/api/getFirstDetailsId')
            .query({ id: true , extraArgument: 172864271827456, id: 83738570375915 })
            .expect(200);
        });

        it('GET /api/getFirstDetailsId/ TIMEOUT 200MS --> Json containing first details id before 200ms', () => {
            return request(app).get('/api/getFirstDetailsId')
            .timeout(200)
            .expect('Content-Type', /json/)
            .expect(200)
        });
    })
})

describe('Filmstrip', () => {
    describe('getThumbnailsByPage', () => {
        it('GET /api/getThumbnailsByPage/ --> Json of 4 Thumbnail Data', () => {
            return request(app).get('/api/getThumbnailsByPage')
            .query({ page: 1 })
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).toEqual(
                    expect.arrayContaining([
                        {
                            id: expect.any(String), thumbnail: expect.any(String), active: expect.any(Boolean)
                        },
                        {
                            id: expect.any(String), thumbnail: expect.any(String), active: expect.any(Boolean)
                        },
                        {
                            id: expect.any(String), thumbnail: expect.any(String), active: expect.any(Boolean)
                        },
                        {
                            id: expect.any(String), thumbnail: expect.any(String), active: expect.any(Boolean)
                        }
                    ])
                );
            });
        });
    
        it('GET /api/getThumbnailsByPage/ --> 404 if not found', () => {
            return request(app).get('/api/getThumbnailsByPage')
            .query({ page: '' })
            .expect(404);
        });

        it('GET /api/getThumbnailsByPage/ DESTRUCTIVE QUERY --> 404', () => {
            return request(app).get('/api/getThumbnailsByPage')
            .query({ page: {id: 383838, anotherArgument: 4585748348273} , extraArgument: 172864271827456 })
            .expect(404);
        });
    
        it('GET /api/getThumbnailsByPage/ TIMEOUT 200MS --> Json of 4 Thumbnail Data before 200ms', () => {
            return request(app).get('/api/getThumbnailsByPage')
            .query({ page: 1 })
            .timeout(200)
            .expect('Content-Type', /json/)
            .expect(200)
        });
    })

    describe('getNumPages', () => {
        it('GET /api/getNumPages/ --> Json containing total number of pages of filmstrip', () => {
            return request(app).get('/api/getNumPages')
            .expect('Content-Type', /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).toEqual(expect.any(Number));
            });
        });
    
        it('GET /api/getNumPages/ --> 0 if templates.json empty', () => {
            if (Object.keys(data).length === 0 || Object.keys(data).length === undefined){
                return request(app).get('/api/getNumPages')
                .expect('Content-Type', /json/)
                .expect(200)
                .then((response) => {
                    expect(response.body).toEqual(0);
                });
            }
        });

        it('GET /api/getNumPages/ DESTRUCTIVE QUERY --> 200, no arguments required', () => {
            return request(app).get('/api/getNumPages')
            .query({ page: {id: 383838, anotherArgument: 4585748348273} , extraArgument: 172864271827456 })
            .expect(200);
        });
    
        it('GET /api/getNumPages/ TIMEOUT 200MS --> Json containing total number of pages of filmstrip before 200ms', () => {
            return request(app).get('/api/getNumPages')
            .timeout(200)
            .expect('Content-Type', /json/)
            .expect(200)
        });
    })
})

const Thumbnail = ({ thumbnail, onSelect }) => {
    return (
        <a key= {`${thumbnail.id}_active`} href="#" className={thumbnail.active ? "active" : ""} title="7112" onClick={(e) => onSelect(e, thumbnail.id)} style = {{maxWidth: 145}}>
            <img key={`${thumbnail.id}_thumbnail`} src={thumbnail.thumbnail !== "" ? `images/thumbnails/${thumbnail.thumbnail}` : ""} alt="7111-m" width="145" height="121" />
            <span key={`${thumbnail.id}_id`}>{thumbnail.id}</span>
        </a>
    );
}
export default Thumbnail;
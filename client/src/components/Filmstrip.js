import Thumbnail from './Thumbnail'
import Next from './Next'
import Previous from './Previous'
import React, { useEffect } from "react";
import {useState } from 'react';
import axios from 'axios';
const Filmstrip = ({updateDetailsId}) => {
    
    const [page, setPage] = useState(1);
    const [savedSelectedId, setSelectedID] = useState(-1);
    const [thumbnails, setThumbnails] = useState([{id: "", thumbnail: "", active: ""}]);

    //Thumbnail Selection 
    const selectThumbnail = (e, selected_id) => {
        e.preventDefault();
        setThumbnails(
            thumbnails.map((thumbnail) =>
                thumbnail.id === selected_id ? { ...thumbnail, active: !thumbnail.active} : { ...thumbnail, active: false}
            )
            
        );
        //Update Details Component Metatdata/Image
        updateDetailsId(selected_id);
        //Save Selected Index in case change page and come back without selecting a new thumbnail
        setSelectedID(selected_id);
    };
    
    //Fill in thumbnail data from api (and check if any were selected)
    const updateThumbnails = (thumbnails_api_data) => {
        setThumbnails(
            thumbnails_api_data.map((thumbnail) =>
                thumbnail.id === savedSelectedId ? { ...thumbnail, active: true} : { ...thumbnail, active: false}
            )
        );
    };
    
    //Forward Backward onClick actions
    const onForward = (e) => {
        e.preventDefault();
        setPage(+page + +1);
    };
    const onBackward = (e) => {
        e.preventDefault();
        setPage(+page - +1);
    };

    //Gets Thumbnails Data from API by page (used more than once)
    async function getThumbnails() {
        let res = await axios.get("/api/getThumbnailsByPage", {
            params: {
                page: page
            }
        })
        .then((res) => updateThumbnails(res.data))
        .catch((error) => {
            console.log(error);
        });
    }

    //Gets Thumbnails Data from API by page
    useEffect(() => {
        getThumbnails();
    }, [page]);

    //Gets Thumbnails Data from API and Selects First Item in JSON on init
    const useMountEffect = () => {
        getThumbnails();
    };

    return (
        <div className="thumbnails">
            <div className="group">
                {thumbnails.map((thumbnail) => (
                    <Thumbnail key={thumbnail.id} thumbnail={thumbnail} onSelect={selectThumbnail}/>
                ))}
                <Previous onBackward={onBackward} page = {page}/>
				<Next onForward={onForward} page = {page}/>
            </div>
        </div>
    );
}
export default Filmstrip;
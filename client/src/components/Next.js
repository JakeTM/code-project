import React, { useEffect } from "react";
import {useState } from 'react';
import axios from 'axios';
const Next = ({ onForward, page }) => {
    //only onForward needs the total number of pages
    const [totalPages, setTotalPages] = useState([1]);
    useEffect(() => {
        async function fetchMyAPI() {
            let res = await axios.get("/api/getNumPages", {
                params: {
                page: page
                }
            })
            .then((res) => setTotalPages(
                res.data
            ))
            .catch((error) => {
                console.log(error);
            }); 
        }
        fetchMyAPI()
    }, [])
    return (
        <a href="#" className={`next ${+page < +totalPages ? `` : `disabled`}`} title="Next" onClick={+page < +totalPages ? (e) => onForward(e) : (e) => e.preventDefault()}>Next</a>
    );
}
export default Next;
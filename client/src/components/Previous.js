const Previous = ({ onBackward, page }) => {
    return (
        <a href="#" className={`previous ${+page > 1 ? `` : `disabled`}`} title="Previous" onClick={+page > 1 ? (e) => onBackward(e) : (e) => e.preventDefault()} >Previous</a>
    );
}
export default Previous;
import React, { useEffect } from "react";
import {useState } from 'react';
import axios from 'axios';
const Details = ({detailsId}) => {

    const [detailsData, setDetailsData] = useState([{cost: "",description: "",id: "",image: "",thumbnail: "",title: ""}]);

    //Gets Main Content Details Data from API by the selected Thumbnail's Id
    useEffect(() => {
        async function fetchMyAPI() {
            if( detailsId !== 0){
                let res = await axios.get("/api/getDetailsById", {
                    params: {
                    id: detailsId
                    }
                })
                .then((res) => setDetailsData(
                    detailsData.map((details) =>
                    details = res.data
                    )
                ))
                .catch((error) => {
                    console.log(error);
                });
            }
        }
        fetchMyAPI();
    }, [detailsId]);

    //Return HTML Filled in With Details
    return (
        <div id = "large">
          <div className="group">
            {detailsData.map((detailsData) => (
                <div key= {`${detailsData.id}_container`}>
                    <img src={detailsData.image !== "" ? `./images/large/${detailsData.image}` : ""} alt="" width="430" height="360" />
                    <div className = "details">
                        <p key= {`${detailsData.id}_title`}><strong>Title</strong> {detailsData.title}</p>
                        <p key= {`${detailsData.id}_description`}><strong>Description</strong> {detailsData.description}</p>
                        <p key= {`${detailsData.id}_cost`}><strong>Cost</strong> {detailsData.cost}</p>
                        <p key= {`${detailsData.id}_id`}><strong>ID #</strong> {detailsData.id}</p>
                        <p key= {`${detailsData.id}_thumbnail`}><strong>Thumbnail File</strong> {detailsData.thumbnail}</p>
                        <p key= {`${detailsData.id}_image`}><strong>Large Image File</strong> {detailsData.image}</p>
                    </div>
                </div>
            ))}
          </div>
        </div>
    );
}
export default Details;
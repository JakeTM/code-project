import './css/style.css';
import {useState } from 'react';
import React, { useEffect } from "react";
import axios from 'axios';
import Filmstrip from './components/Filmstrip';
import Details from './components/Details';

function App() {
  const [detailsId, setDetailsId] = useState(0);
  const updateDetailsId = (detailsId_api_data) => {
    setDetailsId(detailsId_api_data);
  };

  //Gets Main Content Details Id from API on init for first entry in json
  useEffect(() => {
    async function fetchMyAPI() {
        let res = await axios.get("/api/getFirstDetailsId")
        .then((res) => updateDetailsId(res.data))
        .catch((error) => {
            console.log(error);
        });
    }
    fetchMyAPI();
  }, []);

  return (
    <>
      <Details detailsId = {detailsId}/>
      <Filmstrip updateDetailsId = {setDetailsId}/>
    </>
  );
}
export default App;
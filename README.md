# Code Project Submission (Website Template Viewer)

This project is a submission to the code assignment found [here](https://bitbucket.org/mentorg/codeproject/src/master/). The template viewer is modeled after the filmstrip folder view in Windows Explorer. 


---
# Installation

### Project Requirements

- Node.js (tested with v16.13.1 on Windows 10)

### To Install

*  Clone the repository

```
git clone https://JakeTM@bitbucket.org/JakeTM/code-project.git 
```

* Navigate to the root directory

```
cd code-project
```

* Install node dependencies

```
npm install
```


---
# Running the Application

### To Run the Production Build

* Navigate to the root directory (code-project)
* Run the production build on the production server

```
npm run start
```

* Navigate to [http://localhost:5000/](http://localhost:5000/)
 
### To Run the Unit Tests

* Navigate to the root directory (code-project)
* Run the api unit test script

```
npm run test
```

* Note Console Output for Results
* If you plan on running the Unit Tests more than once, set process.env.NODE\_ENV on line 52 of [server.js](https://bitbucket.org/JakeTM/code-project/src/master/server/server.js) to ‘test’
* If you do not and re run the tests, the first test will fail, but the rest will run as intended

### To Run the Development Build

* Navigate to the root directory (code-project)
* Run the development server

```
npm run dev
```

* In a new terminal, navigate to the client directory

```
cd client
```

* Install client node dependencies

```
npm install
```

* Start client server

```
npm run start
```

* Navigate to [http://localhost:3000/](http://localhost:3000/)
* Note: React Components can be found in [code-project/src/master/client/src/components/](https://bitbucket.org/JakeTM/code-project/src/master/client/src/components/)


---
# About the Project

### Features

* Filmstrip of Thumbnails that utilizes a highly scalable pagination system
* Filmstrip slides left and right displaying 4 thumbnails at a time
* Filmstrip Next and Previous Buttons are disabled and not clickable when there are no available pages left or right, respectively
* Filmstrip Thumbnail Image has a border on click, displaying the large image in the main window with its meta data
* Filmstrip Thumbnail Image will keep its border when navigating to a different page and then returning
* Filmstrip Thumbnail Image will remove its border when clicking on the Thumbnail Image a second time; the main window will remain unchanged until a new Thumbnail image is selected
* Application is fully functional when swapping the contents of [templates.json](https://bitbucket.org/JakeTM/code-project/src/master/server/data/templates.json) with different website template json data; This includes the very large ids in [extendedTemplate.json](https://bitbucket.org/JakeTM/code-project/src/master/server/data/extendedTemplate.json)

### Tools and Technologies Used:

* ReactJS
* NodeJS/Express
* Jest
* Supertest
* Nodemon

### File Structure

* [client](https://bitbucket.org/JakeTM/code-project/src/master/client/) Directory: contains react build directory, as well as the development version of the react app \(public and src directories\)
* [server](https://bitbucket.org/JakeTM/code-project/src/master/server/) Directory: contains the data directory \(template json data\) and server.js
* [tests](https://bitbucket.org/JakeTM/code-project/src/master/tests/) Directory: contains the the file [server.test.js](https://bitbucket.org/JakeTM/code-project/src/master/tests/server.test.js), which uses Jest and Supertest to test the nodejs/expressjs backend api
* [package.json](https://bitbucket.org/JakeTM/code-project/src/master/package.json): contains dependencies and run scripts

### React Component Structure/Design:

* The app is split into 2 parent React Components:

    * [Details](https://bitbucket.org/JakeTM/code-project/src/master/client/src/components/Details.js): Main Window. Gets its data from an API after being passed the selected Id via App.js
    * [Filmstrip](https://bitbucket.org/JakeTM/code-project/src/master/client/src/components/Filmstrip.js): Sliding Window. Gets is data from an API on page change. Passes the selected Id to App.js
    
        * [Thumbnail](https://bitbucket.org/JakeTM/code-project/src/master/client/src/components/Thumbnail.js): Individual thumbnail in Filmstrip. Has an active json parameter which triggers the border or not in html
        * [Next](https://bitbucket.org/JakeTM/code-project/src/master/client/src/components/Next.js): Button that on click tells Filmstrip to increment the page by 1, greys out and is not clickable when on the last page
        * [Previous](https://bitbucket.org/JakeTM/code-project/src/master/client/src/components/Previous.js): Button that on click tells Filmstrip to decrement the page by 1, greys out and is not clickable when on the first page
        
    
* Disclaimer: I am new to ReactJS. I am coming from an AngularJS background.

### Project Considerations

* I decided to use React to build the front end of the app because the different sections could be easily component-ized
* There is no empty template for main-data, so on initial application load the first element is displayed

    * The first element does not have a border on initial application load due to the feature requirements stating “the thumbnail image should have a border when selected”. This is an easy change if desired
    
* Because there is no empty template for the main window, when de-selecting a thumbnail I decided that it will not change the main window data
* In a real production repository, the development version of the front end \(client/public and client/src\) would not be included; I've included them in master purely for easy code reference
* For Testing, I made 4 unit tests for each api call, there are many more tests that can and should be implemented.

    * Expected behavior get
    * Expected 404 on get
    * Destructive Query parameters on get
    * Maximum timeout of 200ms on get check

### Known Possible Improvements to Project

* Animation for filmstrip to animate sliding of elements left and right
* Deployment to a cloud service
* More Tests
* CI/CD Pipeline
* Secure over HTTPS

### Thank you for reading

* I enjoyed making this project and am eager to learn and develop using nodejs and react
* If you have any questions for me do not hesitate to reach out